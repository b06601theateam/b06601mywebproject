//importar mongoose
const mongoose = require ('mongoose');

// // modelo esquema (schema) estructura tipo JSON que define la estructura de la base de datos
const ticketSchema = new mongoose.Schema({
    id: Number,
    usuario: String,
    placa: String,
    fechaentrada: String,
    horaentrada:  String

},
{
    versionKey: false,
    timestamps: true
});

 // modelo del producto => tener en cuenta el esquema para la colección...
module.exports = mongoose.model('modelo_tickets', ticketSchema);