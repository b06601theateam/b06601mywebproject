// importar dependencia mongoose
// Importar módulo de MongoDB => mongoose
const mongoose = require('mongoose');

// Importar dependencias variable de entorno
require('dotenv').config({path: 'var.env'})

const conexionDB = async () => {
    try {

        await mongoose.connect(process.env.URL_MONGODB, {});
        console.log('Conexión archivo db.js');

    } catch (error) {
        
        console.log('Error de Conexión a la Base de Datos');
        console.log(error);
        process.exit(1);
    }
}

module.exports = conexionDB;
