const modeloTicket = require('../models/modelTickets');

// Exportar en diferentes variables los métodos para el CRUD

// CRUD => Create
exports.crear = async (req, res) => {
    
    try {
        
        let ticket;

        ticket = new modeloTicket({
            id: 10,
            usuario: "admin",
            placa: "OUK456",
            fechaentrada: "21/12/2021",
            horaentrada: "16:18:00"
         });

        await ticket.save();

        res.send(ticket);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al guardar ticket.');
    }
}


// CRUD => Read
exports.obtener = async (req, res) => {
    try {
        
        const ticket = await modeloTicket.find();
        res.json(ticket);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener el/los ticket(s).');
    }
}
// CRUD => Update
exports.actualizar = async (req, res) => {
    try {
        
        const ticket = await modeloTicket.findById(req.params.id);

        if (!ticket){
            console.log(ticket);
            res.status(404).json({msg: 'El ticket no existe.'});
        }
        else{
            await modeloTicket.findByIdAndUpdate({_id: req.params.id}, {placa:"asdas"});
            res.json({msg: 'Producto actualizado correctamente'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar el ticket.');
    }
}

// CRUD => Delete
exports.eliminar = async (req, res) => {

    try {
        const ticket = await modeloTicket.findById(req.params.id);

        // console.log(ticket);

        if (!ticket){
            console.log(ticket);
            res.status(404).json({msg: 'El ticket no existe.'});
        }
        else{
            await modeloTicket.findByIdAndRemove({_id: req.params.id});
            res.json({mensaje: 'ticket eliminado correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar el ticket.')
    }
}