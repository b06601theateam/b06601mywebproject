//importar express
const express = require('express');

//importar mongoose
//const mongoose = require ('mongoose');

//importar dependencias para variables de entorno
//require('dotenv').config({path: 'var.env'})
const conectarDB = require('./config/db')

//crear var de la app
let app = express();

//var para las rutas
const router = express.Router();


//app.use('/',function(req,res){
//   res.send("Testing testing");
//});

app.use(router);

conectarDB();

//conectar bd con variables de entorno
/* 
mongoose.connect(process.env.URL_MONGODB)
         .then(function(){console.log("Conexión Establecida con MongoDB Atlas")});
         .catch(function(e){console.log(e)}); */



// // modelo esquema (schema) de la base de datos
// const ticketSchema = new mongoose.Schema({
//    id: Number,
//    usuario: String,
//    placa: String,
//    fechaentrada: String,
//    horaentrada:  String
// });
// modelo del ticket, esquema para la colección
// const modeloTicket = mongoose.model('modelo_ticket', ticketSchema);
//  CRUD  Create
/*  modeloTicket.create(
     {
        id: 7,
        usuario: "admin",
        placa: "WIN333",
        fechaentrada: "05/12/2021",
        horaentrada: "15:21:00"
     },
     (error) => {
         if (error) return console.log(error);
    }
);  */


//CRUD CENTRALIZADO
// CRUD Read
//modeloTicket.find((error, ticket) => {
//    if (error) return console.log(error);
//    console.log(ticket);
// });
 

// CRUD Update
/* modeloTicket.updateOne({id: 7}, {placa: "GOO123"}, (error) => {
     if (error) return console.log(error);
 }); */


/* // CRUD Delete
modeloTicket.deleteOne({id: 7}, (error) => {
    if (error) return console.log(error);
 });
 */


// -------------------------------------------
// ######  Rutas Respecto al CRUD  ###########
// -------------------------------------------

// // uso de archivos tipo json en la app
// app.use(express.json());
// CORS (Cross-Origin Resource Sharing) => mecanismo o reglas de seguridad para el control de peticiones http
const cors = require('cors');
app.use(cors());

// solicitudes al CRUD => el controlador 
const crudTickets = require('./controllers/controlTickets');


// Establecer las rutas respecto al CRUD
// CRUD => Create
router.post('/', crudTickets.crear);
// CRUD => Read
router.get('/', crudTickets.obtener);
// CRUD => Update
router.put('/:id', crudTickets.actualizar);
// CRUD => Delete
router.delete('/:id', crudTickets.eliminar);


//metodo get con función normal
//router.get('/metodoget',function(req,res){
//    res.send("Estoy utilizando el método GET");
//});

//metodo get con función fecha
//router.get('/metodoget',(req,res) => {
//    res.send("Estoy utilizando el método GET");
//});

//conexión a base de datos con método post sin usar las var de entorno

//router.post('/metodopost',function(req,res){
//    res.send("Estoy utilizando el método POST");
    /* const user = 'user';
    const psw = 'psw';
    const db = 'elestacionamiento';
    const url = `mongodb+srv://${user}:${psw}@cluster0.avfdx.mongodb.net/${db}?retryWrites=true&w=majority`;
    
    mongoose.connect(url)
     .then(function(){console.log("Conexión Establecida con MongoDB Atlas")})
     .catch(function(e){console.log(e)})
     
     res.send("Estoy utilizando el método POST");
 */
//});


//puerto por el que escucha
app.listen(4000);

console.log("La aplicación se ejecuta en: http://localhost:4000");
